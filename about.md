---
layout: page
title: About Software Freedom Conservancy, Inc.
---

Software Freedom Conservancy, Inc. is a 501(c)(3) not-for-profit organization incorporated in New York. Software Freedom Conservancy helps promote, improve, develop, and defend Free, Libre, and Open Source Software (FLOSS) projects. Conservancy provides a non-profit home and infrastructure for FLOSS projects. This allows FLOSS developers to focus on what they do best — writing and improving FLOSS for the general public — while Conservancy takes care of the projects' needs that do not relate directly to software development and documentation.

FLOSS projects whose [applications are accepted](/members/apply) become part of the Software Freedom Conservancy (akin to a separate department of a large agency). Once joined, the [“member project”](/members) receives most of the benefits of existing as a non-profit corporate entity without engaging in the arduous work of forming a separate, new organization. Conservancy aggregates the work of running a FLOSS non-profit for [its many members](/members/current/).

Conservancy provides [many important services](/members/services/) for its member projects. Member projects can take directed donations, which allows donors to earmark their donations for the benefit of a specific FLOSS project. Conservancy provides fiscal oversight to ensure that these funds are spent in a manner that advances the project and fits with Conservancy's 501(c)(3) mission to promote, advance, and defend software freedom.

If the member project's leaders want, Conservancy can also hold other assets and titles on behalf of the projects, such as copyrights, trademarks, domain names, online hosting accounts, and title and ownership of physical hardware. Also at discretion of the project's leaders, Conservancy can assist in defending the rights represented in these assets. For example, Conservancy is available to assist member projects in enforcing the terms of the projects' FLOSS license.

Finally, developers of Conservancy's member projects, when operating in their capacity as project leaders, could receive some protection from personal liability for their work on the project.

For further reading on the benefits of Conservancy, a full and detailed [list of Conservancy's services for its member projects](/members/services/) and a [a list of Conservancy's current member projects](/members/current/) are available.

Conservancy and its [directors](/about/board), [officers](/about/officers), and [staff](/about/staff) believe strongly in the principles of software freedom, and believe that all users should have the right to study, improve and share their software. Conservancy helps protect, enable, coordinate, facilitate and defend the public's right to copy, share, modify and redistribute FLOSS both non-commercially and commercially. Finally, like most organizations devoted to FLOSS, Conservancy opposes the notion of patents that cover software, and urges contributors to its member projects not to apply for patents.

Conservancy strives to be as transparent as possible, and makes its [public filings available on its website](/about/filings/). Bradley M. Kuhn, Conservancy's President, [blogs regularly](/blog/) about Conservancy's activities. Finally, [detailed information about the work that Conservancy does for its member projects](/members/) is also available.

If you have general questions about Conservancy and its work, [contact information](/about/contact/) is available. Conservancy is primarily supported by [your charitable donations](/donate).
